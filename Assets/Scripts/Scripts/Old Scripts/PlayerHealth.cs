﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    Transform target;
    public static float maxhp;
    public static float curhp;
    public int gold = 10;
    public bool alive = true;
    //public GameObject XPReference;
    private Animator anim;
    NavMeshAgent agent;
    public int vit = 1;
    public int pow = 1;
    public static int levelpoints = 0;
    public Text SkillPointsText;
    public Text vitText;
    public Text powerText;
    public Text hpText;
    public Text dmgText;
	public Text goldText;
	public int level;
    public int damage;
    public static bool statMenu = false;
    public GameObject StatMenuUI;
    public GameObject Target;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();
        maxhp = vit * 100;
        damage = pow * 10;
        curhp = maxhp;
		level = LevelSystem.currentLevel;
    }
    void Update()
    {
        SkillPointsText.text = levelpoints.ToString();
        vitText.text = vit.ToString();
        powerText.text = pow.ToString();
        hpText.text = curhp.ToString() + " / " + maxhp.ToString();
        dmgText.text = damage.ToString();
		goldText.text = gold.ToString();
        maxhp = vit * 100;
        damage = pow * 10;
        if (Input.GetKeyDown("q"))
        {
            //GainHealth(1);
            AddXP(5);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (statMenu == true)
            {
                OpenStats();
            }
            else
            {
                CloseStats();
            }
        }

        if (curhp <= 0)
        {
            curhp = 0;
            alive = false;
            Destroy(gameObject);
        }
        if (!alive)
        {
            return;
        }
    }
    
    public void TakeDamage(float damage)
    {
        HealthBarScript.health -= damage;
        curhp -= damage;
        Debug.Log(damage);
    }
    void OpenStats()
    {
        StatMenuUI.SetActive(false);
        statMenu = false;
    }
    void CloseStats()
    {
        StatMenuUI.SetActive(true);
        statMenu = true;
    }
    public void AddVit()
    {
        if(levelpoints > 0)
        {
            levelpoints--;
            vit++;
            
        }
        else
        {
        }
        
    }
    public void AddPow()
    {
        if (levelpoints > 0)
        {
            levelpoints--;
            pow++;
        }
        else
        {
        }
    }
    public void GainHealth(float heal)
    {
        HealthBarScript.health += heal;
        curhp += heal;
        if(curhp >= maxhp)
        {
            curhp = maxhp;
        }
    }
    public void AddXP(float XP)
    {
        Debug.Log("XP Added:" + XP);
        LevelSystem.XP += XP;
    }
    
	public void AddGold(int addedGold)
	{
		gold += addedGold;
	}
    public void DamageTo()
    {
        target.GetComponent<Health>().TakeDamage(damage);
    }
}
