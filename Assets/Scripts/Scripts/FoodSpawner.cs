﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Boundary
{
    public int xMin, xMax, yMin, yMax;
}
public class FoodSpawner : MonoBehaviour
{
    //Declare variables
    public GameObject Food;
    public float Speed;
    public Boundary boundary;
    public int count;
    private int max = 100;

    //Called when the game begins
    void Start()
    {
        Initial();
        InvokeRepeating("Generate", 0, Speed);

    }

    //This function creates a new food bit
    void Generate()
    {
        if (count <= max)
        {
            //Generate random x and y pixel positions
            int x = Random.Range(boundary.xMin, boundary.xMax);
            int y = Random.Range(boundary.yMin, boundary.yMax);

            //Turn into word space
            Vector3 Target = new Vector3(x, y, 0);
            Target.z = 0;
            Instantiate(Food, Target, Quaternion.identity);
            count++;
        }
    }
    void Initial()
    {
        GameObject prefab = Resources.Load("Food") as GameObject;
        for (count = 0; count < 30; count++)
        {

            int x = Random.Range(boundary.xMin, boundary.xMax);
            int y = Random.Range(boundary.yMin, boundary.yMax);

            //Turn into word space
            Vector3 Target = new Vector3(x, y, 0);
            Target.z = 0;
            GameObject Food = Instantiate(prefab, Target, Quaternion.identity) as GameObject;
        }


    }
    void CountDeduction()
    {
        count--;
    }
}
