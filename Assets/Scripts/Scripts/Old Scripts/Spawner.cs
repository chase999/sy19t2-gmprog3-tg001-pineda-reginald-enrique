﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	public GameObject[] enemies;
	public Vector3 spawnValues;
	public float spawnWait;
	public float spawnMostWait;
	public float SpawnLeastWait;
	public int startWait;
	public bool stop;
	public int maxEnemy;
	int randEnemy;
    public static int enemycount;


    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine (waitSpawner ());
    }

    // Update is called once per frame
    void Update()
    {
		spawnWait = Random.Range (SpawnLeastWait, spawnMostWait);
    }
	IEnumerator waitSpawner()
	{
		yield return new WaitForSeconds(startWait);

		while (!stop) 
		{
                randEnemy = Random.Range(0, enemies.Length);
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 1, Random.Range(-spawnValues.z, spawnValues.z));
            if (enemycount <= maxEnemy)
            {
                Instantiate(enemies[randEnemy], spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
                enemycount++;
            }
            else
            {

            }
                yield return new WaitForSeconds(spawnWait);
            
		}
	}

}
