﻿using UnityEngine;

public class CharacterStats : MonoBehaviour
{
	public int MaxHealth;
	public int currentHealth{ get; set;}
	public int damage;
	public Stat Pow;
	public Stat Vit;


	void Awake()
	{
		currentHealth = MaxHealth;
		MaxHealth = Vit.GetValue() * 100;
		damage = Pow.GetValue() * 10;
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.T))
			{
			TakeDamage (10);
			}
		if (currentHealth >= MaxHealth) 
		{
			currentHealth = MaxHealth;
		}
	}

	public void TakeDamage(int damage)
	{
		currentHealth -= damage;
		Debug.Log (transform.name + " takes " + damage + " damage.");

		if (currentHealth <= 0) 
		{
			Die ();
		}
	}

	public virtual void Die()
	{
		
	}
}
