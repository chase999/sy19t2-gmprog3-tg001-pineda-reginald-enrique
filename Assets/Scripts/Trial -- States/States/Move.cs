﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Move : States
{

    private readonly Player Koko;
    //private bool isMoving;

    public Move(Player koko)
    {
        Koko = koko;
    }

    public void UpdateState()
    {
        MouseClick();
    }
    public void MainMove() { }
    public void TargetAttack()
    {
        Koko.currentState = Koko.kokoT;  //Switch to koko Target script
    }

    private void MouseClick()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //else
        {
            if (Koko.isMoving == true)
            {
                Koko.curState = Player.states.WalkAnim;
                Koko.point.SetActive(true);
                Koko.marker.SetActive(false);
                if (Koko.navMesh.remainingDistance <= Koko.navMesh.stoppingDistance)
                {
                    Koko.isMoving = false;
                }
            }
            if (Koko.isMoving == false)
            {
                Koko.curState = Player.states.IdleAnim;
                Koko.point.SetActive(false);
                Koko.marker.SetActive(false);
            }

            //Mouse movement
            if (Input.GetMouseButton(0))
            {
                if (EventSystem.current.IsPointerOverGameObject()) { }
                else
                if (Physics.Raycast(ray, out hit, 100, Koko.layerMask))
                {
                    Koko.isMoving = true;
                    //move = hit.point;
                    Koko.point.transform.position = hit.point;
                    //Koko.transform.LookAt(move);
                }
            }
            if (Input.GetMouseButton(1))
            {
                Koko.point.SetActive(false);
                Koko.curState = Player.states.DrawBlade;
                TargetAttack();
            }
        }
    }
}