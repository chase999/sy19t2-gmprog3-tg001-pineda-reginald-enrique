﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
	public PlayerHealth P_Health;
    // Start is called before the first frame update
    void Start()
    {
		P_Health = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
	public void gainHealth()
	{
		P_Health.GainHealth(50);
        Destroy(gameObject);
	}

}
