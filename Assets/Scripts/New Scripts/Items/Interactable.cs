﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
	public float radius = 3f;
	public Transform interactionTranform;

	bool isFocus = false;
	Transform player;

	bool HasInteracted = false;

	public virtual  void Interact()
	{
		Debug.Log ("Interacting with " + transform.name);
	}

	void Update()
	{
		if (isFocus && !HasInteracted) 
		{
			float distance = Vector3.Distance (player.position, transform.position);
			if (distance <= radius) 
			{
				HasInteracted = true;
			}
		}
	}
	public void OnFocused(Transform playerTransform)
	{
		isFocus = true;
		player = playerTransform;
		HasInteracted = false;
	}

	public void OnDefocused()
	{
		isFocus = false;
		player = null;
		HasInteracted = false;
	}

	void OnDrawGizmosSelected ()
	{
		if( interactionTranform == null)
		{
			interactionTranform = transform;
		}
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (interactionTranform.position, radius);
	}


}
