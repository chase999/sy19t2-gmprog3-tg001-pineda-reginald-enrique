﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelUp : MonoBehaviour
{

    private Stats kokostat;

    void Start()
    {
        kokostat = GetComponent<Stats>();
    }

    //To be Changed
    public void MainLevelUp()
    {
        if (kokostat.CurrExp >= kokostat.MaxExp)
        {
            kokostat.Level++;
            kokostat.CurrHp = kokostat.MaxHp;
            kokostat.CurrExp = 0;
            kokostat.StatPoints += 2 + kokostat.Level;
        }
    }

    public void MonsterDead(int exp, int gold)
    {
        kokostat.CurrExp += exp;
        kokostat.Gold += gold;
        if (kokostat.CurrExp >= kokostat.MaxExp)
        {
            MainLevelUp();
        }
    }

    public void AddVit()
    {
        if (kokostat.StatPoints > 0)
        {
            kokostat.Vit++;
            kokostat.StatPoints--;
        }
    }

    public void AddStr()
    {
        if (kokostat.StatPoints > 0)
        {
            kokostat.Str++;
            kokostat.StatPoints--;
        }
    }
}
