﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour   
{
    [SerializeField] Transform target;
    private readonly Player Koko;
    private Vector3 move;
    NavMeshAgent agent;
    public GameObject destinationPoint;
    float distance;
    private Animator anim;
    public float attackrange;
    public GameObject selectedUnit;
    private bool moving = false;
    float basetime = 3;
    float timer = 3;
    private float attackDelay = 2f;
    public float lastAttackTime;
    public Health EnemyHealthScript;
    private void Start()
    {
        anim = GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();
        InvokeRepeating("DistanceChecker", 1, 0.25f);
        moving = false;
    }

    void DistanceChecker()
    {
        distance = Vector3.Distance(agent.transform.position, target.transform.position);
    }
    void Update()
    {
        
        timer -= Time.deltaTime;
        if (distance <= 0.2f && timer <= 0)
        {
            destinationPoint.SetActive(false);
        }

        if (Input.GetMouseButtonDown(1))
        {
            SelectTarget();
        }
        else
        {
        }
        //Milestone1

        //Raycast to get the Vector3 of your target point
        // point is alwasy alive in the game
        // when you reach the point turn setactive false
        // when you click in the terrain enable/setactive true your point
        // change the position of the point base on the vector3 from your raycast
        //agent.setdesitination
        //when the reach the point setactive false again 

    }

   
    void BasicAttack()
    {
        //if (Time.time > lastAttackTime + attackDelay)
        //{
            anim.Play("KK_Attack");
            EnemyHealthScript.TakeDamage(GetComponent<PlayerHealth>().damage);
            Debug.Log("Attacking");
           // lastAttackTime = Time.time;
        //}
    }



    void Move()
    {
        timer = basetime;
            destinationPoint.SetActive(true);
            Vector3 destination = Input.mousePosition;
            Ray CastPoint = Camera.main.ScreenPointToRay(destination);
            RaycastHit hit;
            if (Physics.Raycast(CastPoint, out hit, Mathf.Infinity))
            {
                target.transform.position = hit.point;
                moving = false;
            }
            agent.SetDestination(target.position);
    }
    void SelectTarget()
    {
        destinationPoint.SetActive(true);
        Vector3 destination = Input.mousePosition;
        Ray CastPoint = Camera.main.ScreenPointToRay(destination);
        RaycastHit hit;
        if (Physics.Raycast(CastPoint, out hit, Mathf.Infinity))
            {

                if (hit.transform.tag == "Enemy")
                {

                    selectedUnit = hit.transform.gameObject;
                    float distanceToTarget = Vector3.Distance(transform.position, selectedUnit.transform.position);

                    if (distanceToTarget <= attackrange)
                    {

                        Debug.Log("hit");
                        EnemyHealthScript = selectedUnit.GetComponent<Health>();
                        while (EnemyHealthScript.curhp >= 0)
                        {

                            BasicAttack();
                        }
                    }
                }
                else
                {
                    timer = basetime;
                    if (Physics.Raycast(CastPoint, out hit, Mathf.Infinity))
                    {
                        target.transform.position = hit.point;
                        moving = false;
                    }
                    agent.SetDestination(target.position);
                }
            }
    }
    public bool CurrentlyMoving()
    {
        if (distance <= 0.1f)
        {
            return false;
        }
       
        else
        {
            return true;
        }
       
    }

    
}
