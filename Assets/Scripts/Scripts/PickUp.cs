﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour
{
    private Inventoryold inventory;
    public Button itemButton;
    
    void Start()
    {
		
		inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventoryold>();

    }
    
   void AddListeners()
   {
        for (int i = 0; i < inventory.slots.Length; i++)
        {
			itemButton.onClick.AddListener(() => GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().GainHealth(50));
        }
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (inventory.isFull[i] == false)
                {   
                    //Item can be added to Inventory
                    inventory.isFull[i] = true;
                    Instantiate(itemButton, inventory.slots[i].transform, false);
					AddListeners ();
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
    
}
