﻿using UnityEngine;
using System.Collections;

public class Target : States
{

    private readonly Player Koko;
    public GameObject TargetEnemy;

    public Target(Player koko)
    {
        Koko = koko;
    }

    public void UpdateState()
    {
        TargetClick();
    }
    public void MainMove()
    {
        Koko.currentState = Koko.kokoM;
    }
    public void TargetAttack() { }

    private void TargetClick()
    {
        Ray targetRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit targetHit;
        if (Input.GetMouseButton(0))
        {
            Koko.curState = Player.states.PutBlade;
            MainMove();
        }
        if (Input.GetMouseButton(1))
        {
            if (Physics.Raycast(targetRay, out targetHit))
            {
                if (targetHit.collider.tag == "Enemy")
                {
                    TargetEnemy = targetHit.collider.gameObject;
                    Koko.TargetMoving = true;
                }
            }
        }
        if (Koko.TargetMoving == true)
        {
            Koko.curState = Player.states.AttackRun;
            Koko.point.SetActive(false);
            Koko.marker.SetActive(true);
            Koko.marker.transform.position = TargetEnemy.transform.position;
            Koko.navMesh.destination = Koko.marker.transform.position;
            if (Koko.navMesh.remainingDistance <= 1.5)
            {
                Koko.curState = Player.states.AttackAnim;
                Attack(1);
            }
        }
        if (Koko.TargetMoving == false)
        {
            Koko.marker.SetActive(false);
        }
    }

    public void Attack(int dmg)
    {
        dmg = Koko.gameObject.GetComponent<Stats>().Attack;
        TargetEnemy.gameObject.GetComponent<Stats>().CurrHp -= dmg;
        //TargetEnemy.gameObject.GetComponent<Health>().healthBar.value = TargetEnemy.gameObject.GetComponent<Stats>().CurrHp -= dmg; ;
        Debug.Log("Koko dealt " + dmg + "dmg");
        if (TargetEnemy.gameObject.GetComponent<Stats>().CurrHp <= 0)
        {
            Koko.TargetMoving = false;
            Koko.curState = Player.states.PutBlade;
        }
    }

}