﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIStatManager : Stats
{
    //For Stats
    public Text NameText;
    public Text LvlText;
    public Text HPText;
    public Text AttackText;
    public Text VitText;
    public Text StrText;
    public Text ExpText;
    public Text GoldText;
    public Text MPText;
    public Text StatPointsText;

    public void ShowStats()
    {
        NameText.text = "Name: " + Name;
        LvlText.text = "Level: " + Level;
        HPText.text = "HP: " + CurrHp + "/" + MaxHp;
        MPText.text = "MP: " + CurrMp + "/" + MaxMp;
        AttackText.text = "Attack: " + Attack;
        VitText.text = "Vit: " + Vit;
        StrText.text = "Str: " + Str;
        ExpText.text = "Exp: " + CurrExp;
        StatPointsText.text = "Stat Points: " + StatPoints;
        GoldText.text = "Gold: " + Gold;
    }

}
