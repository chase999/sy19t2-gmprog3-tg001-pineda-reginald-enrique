﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSystem : MonoBehaviour
{
    Image Exp;
    public static float XP;
    public float MaxXP = 100;
    public static int currentLevel = 1;
    public GameObject Main;
    public Text Level;
    // Start is called before the first frame update
    void Start()
    {
        Exp = GetComponent<Image>();
        XP = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Level.text = currentLevel.ToString();
        UpdateXP();
        
        Exp.fillAmount = XP / MaxXP;
    }
    public void UpdateXP()
    {
        if (XP >= MaxXP)
        {
            XP -= MaxXP;
            MaxXP += 100;
            currentLevel++;
			PlayerHealth.levelpoints += 2;
            PlayerHealth.curhp = PlayerHealth.maxhp;
        }
    }
	public void AddXP(float exp)
	{
		XP += exp;
	}

}
