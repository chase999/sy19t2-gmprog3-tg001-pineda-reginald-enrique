﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
	public override void Die()
	{
		//death animation
		base.Die();
		Destroy (gameObject);
	}
}
