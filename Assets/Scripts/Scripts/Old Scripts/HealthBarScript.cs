﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    Image HealthBar;
    float maxhealth;
    //  public PlayerHealth PlayerHealthScript;
    public static float health;

    // Start is called before the first frame update
    void Start()
    {
		
        HealthBar = GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
		maxhealth = PlayerHealth.maxhp;
		health = PlayerHealth.curhp;
        HealthBar.fillAmount = health / maxhealth;
    }
}
