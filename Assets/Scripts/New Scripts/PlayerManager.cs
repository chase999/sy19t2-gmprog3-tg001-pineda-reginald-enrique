﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
	#region Singleton

	public static PlayerManager instance;

	void Awake()
	{
		instance = this;
	}

	#endregion

	public GameObject player;

	PlayerStats myStats;

	public void Start()
	{
		myStats = instance.player.GetComponent<PlayerStats>();
	}

	public void GainHealth()
	{
		myStats.currentHealth += 50; 
	}

	public void GainGold(int value)
	{
		myStats.gold += value;
	}
}
