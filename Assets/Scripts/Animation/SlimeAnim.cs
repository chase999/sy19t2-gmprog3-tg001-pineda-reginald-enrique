﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAnim : MonoBehaviour {

    private Animator anim;
    public GameObject slime;
    // Use this for initialization
    void Start()
    {

        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SlimeAtk()
    {
        anim.Play("SL_Attack", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeCombo()
    {
        anim.Play("SL_Attack01", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeTakeDamage()
    {
        anim.Play("SL_Damage", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeDeath()
    {
        anim.Play("SL_Dead", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeIdle()
    {
        anim.Play("SL_Idle", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeRun()
    {
        anim.Play("SL_Run", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeWalk()
    {
        anim.Play("SL_Walk", -1, 0f);
        anim.speed = 1f;
    }
    public void SlimeActive()
    {
        slime.SetActive(true);
    }
    public void SlimeInactive()
    {
        slime.SetActive(false);
    }
}
