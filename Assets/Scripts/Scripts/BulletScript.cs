﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	private Transform target;

	public float speed = 10f;
    public float damage = 5f;
	public void Seek (Transform _target)
	{
		target = _target;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (target == null) 
		{
			Destroy (gameObject);
			return;
		}

		Vector3 dir = target.position - transform.position;
		float distanceThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distanceThisFrame) 
		{
			HitTarget ();
			return;
		}

		transform.Translate (dir.normalized * distanceThisFrame, Space.World);

	}
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Main")
        {

        }
        else
        {
            Destroy(other.gameObject);
        }
        
    }
	void HitTarget()
	{
        
		Destroy (gameObject);
	}

}
