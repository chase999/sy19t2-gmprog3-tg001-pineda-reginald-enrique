﻿using UnityEngine;
using System.Collections;

public interface States
{

    void UpdateState();
    void MainMove();
    void TargetAttack();
}
