﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpGold : MonoBehaviour
{
	private PlayerManager gold;
	public int goldValue;
	void Start()
	{

		gold = PlayerManager.instance;

	}
	// Update is called once per frame
	private void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			gold.GainGold (goldValue);
			Destroy (gameObject);
		}
	}

}
