﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpBarScript : MonoBehaviour
{
    Image Exp;
    float maxExp = 100;
    public static float exp;
    // Start is called before the first frame update
    void Start()
    {
        Exp = GetComponent<Image>();
        exp = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Exp.fillAmount = exp / maxExp;
    }
}
