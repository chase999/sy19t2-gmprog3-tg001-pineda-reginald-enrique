﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
	public GameObject goldPrefab;
	public GameObject Hpotion;
	public GameObject Mpotion;
	public PlayerHealth Player;
    public float maxhp;
	public float vit;
	public float vitModifier;
	public float curhp;
	public float xpValue;
	public float xpModifier;
    public bool alive = true;
	int randDrop;
    // Use this for initialization
    void Start()
    {
		Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
		maxhp = (vit + Player.level)* vitModifier;
		xpValue = 20 + Player.level * xpModifier;
        curhp = maxhp;

	}
	void Update()
	{

		if (curhp <= 0)
		{
			//DropRate ();
            Spawner.enemycount--;
			Debug.Log("EXP Add");
			Player.AddXP (xpValue);
			curhp = 0;
			alive = false;
			Destroy(gameObject);
		}
		if (!alive)
		{
			return;
		}
	}

    public void TakeDamage(float damage)
    {
        curhp -= damage;
		print("Damage Dealt to the enemy is " + damage);
    }

	public void DropGold()
	{
		Instantiate ( goldPrefab, gameObject.transform.position, transform.rotation);

	}
	public void DropHPotion()
	{
		Instantiate( Hpotion, gameObject.transform.position, transform.rotation);

	}

	public void DropMPotion()
	{
		Instantiate( Mpotion, gameObject.transform.position, transform.rotation);

	}

	public void DropRate() 
	{
        randDrop = Random.Range (0, 7);
		if (randDrop == 0)
		{
			DropGold ();
		}
		else if(randDrop == 1)
		{
			DropHPotion ();
		}
		else if(randDrop == 2)
		{
			DropMPotion ();
		}
		else if (randDrop == 3)
		{
			DropGold ();
			DropHPotion ();
		}
		else if (randDrop == 4)
		{
			DropGold ();
			DropMPotion ();
		}
		else if (randDrop == 5)
		{
			DropHPotion ();
			DropHPotion ();
		}
		else if (randDrop == 6)
		{
			DropGold ();
			DropHPotion ();
			DropMPotion ();
		}
    }
}
