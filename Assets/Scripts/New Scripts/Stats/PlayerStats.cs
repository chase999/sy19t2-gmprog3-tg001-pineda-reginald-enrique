﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : CharacterStats
{
	public Text SkillPointsText;
	public Text vitText;
	public Text powerText;
	public Text hpText;
	public Text dmgText;
	public Text goldText;
	public int gold;
	public static int levelpoints;

	public void Update()
	{
		SkillPointsText.text = levelpoints.ToString();
		vitText.text = Vit.GetValue().ToString ();
		powerText.text = Pow.GetValue().ToString();
		hpText.text = currentHealth.ToString() + " / " + MaxHealth.ToString();
		dmgText.text = damage.ToString();
		goldText.text = gold.ToString();
		MaxHealth = Vit.GetValue() * 100;
		damage = Pow.GetValue() * 10;
	}
	public void AddVit()
	{
		if(levelpoints > 0)
		{
			levelpoints--;
			Vit.baseValue++;

		}
		else
		{
		}

	}
	public void AddPow()
	{
		if (levelpoints > 0)
		{
			levelpoints--;
			Pow.baseValue++;
		}
		else
		{
		}
	}
	public override void Die ()
	{
		base.Die ();
	}
}
