﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
	public bool statWindowOpen = false;
	public bool inventoryWindowOpen = false;
	public GameObject statwindowui;
	public GameObject inventoryUI;
    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown (KeyCode.I)) 
		{
			if (inventoryWindowOpen == true) 
			{
				closeInventory ();
			} 
			else 
			{
				openInventory ();
			}
		}
		
		if (Input.GetKeyDown (KeyCode.P)) 
		{
			if (statWindowOpen == true) 
			{
				closeStat ();
			} 
			else 
			{
				openStat ();
			}
		}
    }
	void openStat()
	{
		statwindowui.SetActive (true);
		statWindowOpen = true;
	}
	void closeStat()
	{
		statwindowui.SetActive (false);
		statWindowOpen = false;
	}
	void openInventory()
	{
		inventoryUI.SetActive (true);
		inventoryWindowOpen = true;
	}
	void closeInventory()
	{
		inventoryUI.SetActive (false);
		inventoryWindowOpen = false;
	}
}
