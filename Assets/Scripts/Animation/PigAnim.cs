﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigAnim : MonoBehaviour {

    private Animator anim;
    public GameObject pig;
    // Use this for initialization
    void Start()
    {

        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PigAtk()
    {
        anim.Play("WP_Attack", -1, 0f);
        anim.speed = 1f;
    }
    public void PigCombo()
    {
        anim.Play("WP_Attack01", -1, 0f);
        anim.speed = 1f;
    }
    public void PigTakeDamage()
    {
        anim.Play("WP_Damage", -1, 0f);
        anim.speed = 1f;
    }
    public void PigDeath()
    {
        anim.Play("WP_Dead", -1, 0f);
        anim.speed = 1f;
    }
    public void PigIdle()
    {
        anim.Play("WP_Idle", -1, 0f);
        anim.speed = 1f;
    }
    public void PigRun()
    {
        anim.Play("WP_Run", -1, 0f);
        anim.speed = 1f;
    }
    public void PigWalk()
    {
        anim.Play("WP_Walk", -1, 0f);
        anim.speed = 1f;
    }

    public void PigActive()
    {
        pig.SetActive(true);
    }
    public void PigInactive()
    {
        pig.SetActive(false);
    }
}
