﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gold", menuName = "Inventory/Gold")]
public class Gold : Item
{
	public int value;
	public override void Use ()
	{
		PlayerManager.instance.GainGold (value);
		RemoveFromInventory ();
		Debug.Log("Potion Used");
	}
}
