﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinAnimation : MonoBehaviour {

    private Animator anim;
    public GameObject goblin;
    // Use this for initialization
    void Start()
    {

        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GoblinAtk()
    {
        anim.Play("GB_Attack", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinCombo()
    {
        anim.Play("GB_Attack01", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinTakeDamage()
    {
        anim.Play("GB_Damage", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinDeath()
    {
        anim.Play("GB_Dead", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinIdle()
    {
        anim.Play("GB_Idle", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinRun()
    {
        anim.Play("GB_Run", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinWalk()
    {
        anim.Play("GB_Walk", -1, 0f);
        anim.speed = 1f;
    }
    public void GoblinActive()
    {
        goblin.SetActive(true);
    }
    public void GoblinInactive()
    {
        goblin.SetActive(false);
    }
}
