﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    //Declare variables
    public GameObject Enemy;
    public float Speed;
    public Boundary boundary;
    public int count;
    private int max = 100;

    //Called when the game begins
    void Start()
    {
        InvokeRepeating("Generate", 1f, Speed);

    }
    void Update()
    {
    }
    //This function creates a new food bit
    void Generate()
    {
        if (count <= max)
        {
            //Generate random x and y pixel positions
            int x = Random.Range(boundary.xMin, boundary.xMax);
            int y = Random.Range(boundary.yMin, boundary.yMax);

            //Turn into word space
            Vector3 Target = new Vector3(x, y, 0);
            Target.z = 0;
            Instantiate(Enemy, Target, Quaternion.identity);
            count++;
        }

    }

}