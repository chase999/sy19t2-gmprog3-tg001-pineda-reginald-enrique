﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public Animator anim;
    public LayerMask layerMask;
    public GameObject point;
    public GameObject marker;

    public GameObject enemy;

    [HideInInspector]
    public States currentState;
    [HideInInspector]
    public UnityEngine.AI.NavMeshAgent navMesh;
    [HideInInspector]
    public Move kokoM;

    public Target kokoT;
    [HideInInspector]
    public Exp kokoExp;
    [HideInInspector]
    public bool TargetMoving;
    [HideInInspector]
    public bool isMoving;
    [HideInInspector]
    public bool isIdle;
    [HideInInspector]
    public Stats stat;

    public enum states
    {
        WalkAnim, AttackAnim,
        AttackRun, IdleAnim,
        DeadAnim, SkillAnim,
        DrawBlade, PutBlade
    }
    public states curState;

    public void checkState()
    {
        switch (curState)
        {
            case states.WalkAnim:
                anim.Play("KK_Run_No");
                break;
            case states.IdleAnim:
                anim.Play("KK_Idle");
                break;
            case states.AttackAnim:
                anim.Play("KK_Attack");
                break;
            case states.AttackRun:
                anim.Play("KK_Run");
                break;
            case states.DeadAnim:
                anim.Play("KK_Dead");
                break;
            case states.SkillAnim:
                anim.Play("KK_Skill");
                break;
            case states.DrawBlade:
                anim.Play("KK_DrawBlade");
                break;
            case states.PutBlade:
                anim.Play("KK_PutBlade");
                break;
        }
    }
    private void Awake()
    {
        navMesh = GetComponent<UnityEngine.AI.NavMeshAgent>();
        kokoM = new Move(this);
        kokoT = new Target(this);
    }

    void Start()
    {
        point.SetActive(false);
        marker.SetActive(false);
        stat = GetComponent<Stats>();
        kokoExp = GetComponent<Exp>();
        anim = GetComponent<Animator>();
        currentState = kokoM;
    }

    void Update()
    {
        checkState();
        currentState.UpdateState();
        Dead();
    }

    public void Dead()
    {
        if (stat.CurrHp <= 0)
        {
            stat.CurrHp = 0;              //To avoid negative hp / limit
            curState = states.DeadAnim;
            isMoving = false;
            TargetMoving = false;
            Destroy(gameObject, 3);
            Application.Quit();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemy = other.gameObject;
        }
    }
    void StartEffect()
    { }
    void StopEffect() { }
    void DamageTo() { }
}
