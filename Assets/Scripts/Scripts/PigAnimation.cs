﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigAnimation : MonoBehaviour
{

    private Rigidbody rb;
    public State curState;
    public Boundary boundary;
    public float checkdist;
    public float Speed;
    public Transform[] moveSpots;
    private int randomSpot;
    public float awarenessRange;
    public float attackRange;
    public float distanceToTarget;
    private float lastAttackTime;
    public float attackDelay = 3f;
    public float powModifier;
    public float damage;
    private float dist;
    public float pow;
    //private int x = 0;
    //private int y = 0;
    //private Vector3 Target;
    public Transform Player;
    public PlayerHealth level;
    public float waitTime;
    public float StartWaitTime;
    private Animator anim;
    void Start()
    {
        level = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        waitTime = StartWaitTime;
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        damage = (pow + level.level) * powModifier;
        RandomizedPatrol();
    }
    void Update()
    {
        //add a null checker for the player
        if (Player != null)
        {
            distanceToTarget = Vector3.Distance(transform.position, Player.position);


            //Check Distance to Player


            //Check to see if the enemy is aware of the player - if not then patrol
            if (distanceToTarget > awarenessRange)
            {
                Run();
                UpdatePatrol();
            }

            // if the player is within the enemyś awareness Range. - Chase
            if (distanceToTarget < awarenessRange && distanceToTarget > attackRange)
            {
                
                UpdateRun();
            }

            if (distanceToTarget < attackRange)
            {
                
                UpdateAttack();
            }

            /*
            if (Target == transform.position)
            {
                x = Random.Range(boundary.xMin, boundary.xMax);
                y = Random.Range(boundary.yMin, boundary.yMax);
            }
            switch (curState)
            {
                case State.Patrol: UpdatePatrol(); break;
                case State.Run: UpdateRun(); break;
                case State.Attack: UpdateAttack(); break;
            case State.Idle: UpdateIdle (); break;
            }
            */
        }

    }

    void RandomizedPatrol()
    {
        randomSpot = Random.Range(0, moveSpots.Length);
    }
    void UpdateAttack()
    {
        //Attacking AI - Melee
        if (Time.time > lastAttackTime + attackDelay)
        {
            Attack();
            if (Player != null)
            {
                Player.SendMessage("TakeDamage", damage);
                //Record the time we attacked
                lastAttackTime = Time.time;
            }
        }

    }

    void UpdateRun()
    {
        if (Player != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.position, Speed * Time.deltaTime);
            Run();
        }

    }

    void UpdatePatrol()
    {
        transform.position = Vector3.MoveTowards(transform.position, moveSpots[randomSpot].position, Speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, moveSpots[randomSpot].position) < 2f)
        {
            if (waitTime <= 0)
            {
                RandomizedPatrol();
                waitTime = StartWaitTime;
            }
            else
            {
                
                UpdateIdle();
            }
        }
        /*
		 Target = new Vector3(x, y);
        transform.position = Vector3.MoveTowards(transform.position, Target, -1* Speed * Time.deltaTime / transform.localScale.x);
        rb = GetComponent<Rigidbody>();
        rb.position = new Vector3
            (
                Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
                Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax),
                0.0f

            );
		
        Target = new Vector3(x, y);
        transform.position = Vector3.MoveTowards(transform.position, Target, Speed * Time.deltaTime / transform.localScale.x);
        */
    }

    void UpdateIdle()
    {
        Idle();
        waitTime -= Time.deltaTime;
    }
  
   

    public void Idle()
    {
        anim.Play("WP_Idle", -2, 0f);
    }
    public void Run()
    {
        anim.Play("WP_Run", -2, 0f);
    }

    public void Attack()
    {
        anim.Play("WP_Attack", -2, 0f);
    }
    // Start is called before the first frame update
    
}
