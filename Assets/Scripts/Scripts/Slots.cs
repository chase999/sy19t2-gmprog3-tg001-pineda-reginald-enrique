﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slots : MonoBehaviour
{
	private Inventoryold inventory;
	public int i;
	public void Start()
	{
		inventory = GameObject.FindGameObjectWithTag ("Player").GetComponent<Inventoryold> ();
	}

	private void Update()
	{
		if(transform.childCount <= 0)
		{
			inventory.isFull [i] = false;
		}
	}

	public void DropItem()
	{
		foreach (Transform child in transform) 
		{
			GameObject.Destroy (child.gameObject);
		}
	}
}
