﻿using UnityEngine;
using System.Collections;

public class FoodBound : MonoBehaviour {
	private Rigidbody rb;
	public Boundary boundary;

	void Update()
	{
		rb = GetComponent<Rigidbody>();
		rb.position = new Vector3 
			(
				Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
				Mathf.Clamp (rb.position.y, boundary.yMin, boundary.yMax),
				0.0f
			);

	}

}
