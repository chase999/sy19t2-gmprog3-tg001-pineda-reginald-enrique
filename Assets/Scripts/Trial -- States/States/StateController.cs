﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class StateController : MonoBehaviour
{

    private Vector3 movement;
    public GameObject pointer;
    public GameObject marker;
    NavMeshAgent agent;
    Animator animator;
    public GameObject TargetEnemy;
    public bool Target = false;
    public bool Dead = false;
    public bool InRange = false;

    public Stats playerStats;

    public enum States
    {
        Walk, Attack,
        AttackRun, Idle,
        Dead, Skill,
        DrawBlade, PutBlade
    }
    public States curState;

    public void checkState()
    {
        switch (curState)
        {
            case States.Walk:
                animator.Play("KK_Run_No");
                break;
            case States.Idle:
                animator.Play("KK_Idle");
                break;
            case States.Attack:
                animator.Play("KK_Attack");
                TargetAttack();
                break;
            case States.AttackRun:
                animator.Play("KK_Run");
                break;
            case States.Dead:
                animator.Play("KK_Dead");
                break;
            case States.Skill:
                animator.Play("KK_Skill");
                break;
            case States.DrawBlade:
                animator.Play("KK_DrawBlade");
                break;
            case States.PutBlade:
                animator.Play("KK_PutBlade");
                break;
        }
    }
    // Use this for initialization
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        playerStats = GetComponent<Stats>();
    }


    // Update is called once per frame
    void Update()
    {
        Death();
        checkState();
    }

    public void Death()
    {
        if (playerStats.CurrHp <= 0)
        {
            playerStats.CurrHp = 0; // avoid negative hp
            curState = States.Dead;
            Destroy(gameObject, 3);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
             TargetEnemy = other.gameObject;
        }
    }

    public void OnMouseClick()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (playerStats.CurrHp > 0)
                    {
                        TargetEnemy = hit.collider.gameObject;
                        //target
                        if (hit.collider.tag == "Enemy")
                        {
                            pointer.SetActive(false);
                            marker.SetActive(true);
                            TargetEnemy = hit.transform.gameObject;
                            if (InRange)
                            {
                                curState = States.Attack;
                            }
                            else if (!InRange)
                            {
                                curState = States.AttackRun;
                                movement = TargetEnemy.transform.position;
                                transform.LookAt(movement);
                            }

                        }
                    }
                    movement = hit.point;
                    pointer.transform.position = hit.point;
                    pointer.SetActive(true);
                    agent.destination = movement;

                    animator.Play("KK_Run_No");
                    //stop focus
                    //RemoveFocus();
                }
            }
        }


    }

    public void TargetAttack()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray targetRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Hit;
            if (Physics.Raycast(targetRay, out Hit))
            {



                {
                    animator.Play("KK_DrawBlade");
                    animator.Play("KK_AttackRun");
                    pointer.SetActive(false);
                    marker.SetActive(true);
                    marker.transform.position = TargetEnemy.transform.position;
                    agent.destination = marker.transform.position;
                    if (agent.remainingDistance <= 1.5)
                    {
                        animator.Play("KK_Attack");
                        //Debug.Log("attack");
                        Attack(5);

                    }
                }

            }
        }
    }
    private void Attack(int dmg)
    {
        TargetEnemy.gameObject.GetComponent<Stats>().CurrHp -= dmg;
        if (TargetEnemy.gameObject.GetComponent<Stats>().CurrHp <= 0)
        {
            Debug.Log("deal 5 damage");
            animator.Play("KK_PutBlade");
        }
    }
}
