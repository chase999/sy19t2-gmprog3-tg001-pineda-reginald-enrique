﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimation : MonoBehaviour {

    private Animator anim;
    public GameObject npc;
    // Use this for initialization
    void Start()
    {
        
        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void NPCIdle()
    {
        anim.Play("Idle", -1, 0f);
        anim.speed = 1f;
    }
    public void NPCTalk()
    {
        anim.Play("talk", -1, 0f);
        anim.speed = 1f;
    }
    public void NPCActive()
    {
        npc.SetActive(true);
    }
    public void NPCInactive()
    {
        npc.SetActive(false);
    }
}
