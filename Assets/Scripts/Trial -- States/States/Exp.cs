﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Exp : MonoBehaviour
{

    [HideInInspector]
    private Stats kokoStats;
    public Slider ExpGauge;
    public Text ExpUpdate;
    private UIStatManager showstats;

    void Start()
    {
        kokoStats = GetComponent<Stats>();
        showstats = GetComponent<UIStatManager>();
        ExpGauge.maxValue = kokoStats.CurrExp;
    }

    void Update()
    {
        showstats.ShowStats();
        expGauge();
    }

    public void expGauge()
    {
        if (gameObject.tag == "Koko")
        {
            ExpGauge.maxValue = kokoStats.MaxExp;
            ExpGauge.value = kokoStats.CurrExp;
            ExpUpdate.text = kokoStats.CurrExp + "/" + kokoStats.MaxExp;

        }
    }
}
