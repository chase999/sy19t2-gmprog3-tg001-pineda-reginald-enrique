﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoAnim : MonoBehaviour {
    private Animator anim;
    public GameObject player;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void AnimIdle()
    {
        anim.Play("KK_Idle", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimRun()
    {
        anim.Play("KK_Run_No", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimAtkRun()
    {
        anim.Play("KK_Run", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimAtkStandby()
    {
        anim.Play("KK_Attack_Standy", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimCombo()
    {
        anim.Play("KK_Combo", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimTakeDamage()
    {
        anim.Play("KK_Damage", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimDeath()
    {
        anim.Play("KK_Dead", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimDrawBlade()
    {
        anim.Play("KK_DrawBlade", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimPutBlade()
    {
        anim.Play("KK_PutBlade", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimSkill()
    {
        anim.Play("KK_Skill", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimAttack()
    {
        anim.Play("KK_Attack", -1, 0f);
        anim.speed = 1f;
    }
    public void AnimActive()
    {
        player.SetActive(true);
    }
    public void AnimInactive()
    {
        player.SetActive(false);
    }
}
