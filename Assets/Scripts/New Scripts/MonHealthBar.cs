﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonHealthBar : MonoBehaviour
{
    Image HealthBar;
    float maxhealth;
    public Health HealthScript;
    public float health;

    // Start is called before the first frame update
    void Start()
    {

        HealthBar = GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
        maxhealth = HealthScript.maxhp;
        health = HealthScript.curhp;
        HealthBar.fillAmount = health / maxhealth;
    }
}
