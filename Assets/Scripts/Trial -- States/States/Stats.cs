﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour
{
    LevelUp lvlUp;
    public string Name;
    public int Level;
    public int MaxHp;
    public int CurrHp;
    public int CurrMp;
    public int MaxMp;
    public int Attack;
    public int Vit;
    public int Str;
    public int MaxExp;
    public int CurrExp;
    public int Gold;
    public int StatPoints;

    void Start()
    {
        lvlUp = GetComponent<LevelUp>();
    }
    void Update()
    {
        if (CurrMp < 0)
        {       //Limit
            CurrMp = 0;
        }
        if (gameObject.tag == "Koko")
        {
            Attack = Str * 2;
            MaxHp = Vit * 10;
            if (CurrMp <= 0)
            {
                CurrMp = 0;
            }

            if (CurrHp > MaxHp)
            {
                CurrHp = MaxHp;
            }
            if (CurrExp >= 100)
            {
                lvlUp.MainLevelUp();
            }
        }
        if (gameObject.tag == "Enemy")
        {
            MaxHp = Vit * 10;
            Attack = Str * 1;

        }
        if (Input.GetKeyDown("space"))
        {
            Gold++;
        }
        if (Input.GetKeyDown("m"))
        {
            CurrMp--;
        }
    }
}
