﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Health Potion", menuName = "Inventory/healthPotion")]
public class HealthPotion : Item
{
	public override void Use ()
	{
		PlayerManager.instance.GainHealth ();
		RemoveFromInventory ();
		Debug.Log("Potion Used");
	}
}
